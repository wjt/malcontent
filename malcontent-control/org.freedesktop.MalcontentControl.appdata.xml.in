<?xml version="1.0" encoding="UTF-8"?>
<!-- Copyright © 2019 Endless Mobile, Inc. -->
<component type="desktop">
  <id>org.freedesktop.MalcontentControl</id>
  <metadata_license>CC-BY-SA-3.0</metadata_license>
  <project_license>GPL-2.0+</project_license>

  <!-- Translators: the name of the application as it appears in a software center -->
  <name>Parental Controls</name>

  <!-- Translators: the brief summary of the application as it appears in a software center. -->
  <summary>Set parental controls and monitor usage by users</summary>
  <description>

    <!-- Translators: These are the application description paragraphs in the AppData file. -->
    <p>
      Manage users’ parental controls restrictions, controlling how long they
      can use the computer for, what software they can install, and what
      installed software they can run.
    </p>
  </description>
  <!--
  <screenshots>
    <screenshot type="default" width="400" height="480">
      <image>https://FIXME/malcontent-control.png</image>
    </screenshot>
  </screenshots>
  -->
  <provides>
    <binary>malcontent-control</binary>
  </provides>
  <launchable type="desktop-id">org.freedesktop.MalcontentControl.desktop</launchable>
  <url type="homepage">https://gitlab.freedesktop.org/pwithnall/malcontent</url>
  <url type="bugtracker">https://gitlab.freedesktop.org/pwithnall/malcontent/issues</url>
  <url type="donation">http://www.gnome.org/friends/</url>
  <url type="translate">https://wiki.gnome.org/TranslationProject/LocalisationGuide</url>
  <update_contact>philip_at_tecnocode.co.uk</update_contact>
  <project_group>GNOME</project_group>
  <developer_name>The GNOME Project</developer_name>
  <kudos>
    <kudo>AppMenu</kudo>
    <kudo>HighContrast</kudo>
    <kudo>ModernToolkit</kudo>
  </kudos>
  <translation type="gettext">malcontent</translation>
  <releases>
    <release version="0.7.0" date="2020-03-24" type="stable">
      <description>
        <ul>
          <li>Minor improvements to parental controls application UI</li>
          <li>Translations to Ukrainian and Polish</li>
        </ul>
      </description>
    </release>
    <release version="0.6.0" date="2020-02-26" type="stable">
      <description>
        <ul>
          <li>Improve parental controls application UI and add icon</li>
          <li>Support for indicating which accounts are parent accounts</li>
        </ul>
      </description>
    </release>
    <release version="0.5.0" date="2020-02-14" type="stable">
      <description>
        <ul>
          <li>Initial release of basic parental controls application</li>
          <li>Support for setting app installation and run restrictions on users</li>
        </ul>
      </description>
    </release>
    <release version="0.4.0" date="2019-07-17" type="stable">
      <description>
        <ul>
          <li>Maintenance release of underlying parental controls library</li>
        </ul>
      </description>
    </release>
  </releases>
  <content_rating type="oars-1.1"/>
</component>
